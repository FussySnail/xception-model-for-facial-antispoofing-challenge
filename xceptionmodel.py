"""This file implements a class for training an convolution neural network.
"""

from keras.callbacks import ModelCheckpoint
from keras.engine import InputLayer
from keras.layers import Dense, GlobalAveragePooling2D
from keras.models import Model
from keras.applications import Xception
from pathlib import Path
import numpy as np
import cv2
import psutil
import sys
from keras import backend as k


class XceptionModel:
    """This class represents basic methods for manipulating neural network"""

    def __init__(self,
                 nb_classes,

                 frame_width=224,
                 frame_height=224,

                 channels_num=3,

                 weights_folder=None,
                 batch_size=1,
                 nb_epoch=10,
                 lr=0.0003,
                 normalize=True,
                 frame_count=1,
                 loss="categorical_crossentropy",
                 metrics=["categorical_accuracy"],
                 monitor_callback="val_categorical_accuracy",
                 save_best_only=True,
                 save_weights_only=True,
                 verbose_callback=True,
                 optimizer="adam",
                 img_dtype="float32",
                 model_name='neural_network'):
        """Initializes basic network parameters for easy fitting;
            :param nb_classes: (int) number of classes;
            :param frame_width: (int) image width;
            :param frame_height: (int) image height;
            :param channels_num: (int) number of frame channels;
            :param batch_size: (int) size of batch;
            :param nb_epoch: (int) number of epochs;
            :param lr: (float) learning rate;
            :param weights_folder: (str) absolute path to weights folder; folder must exist;
            :param loss: (str) loss function;
            :param metrics: (list of strings): list of metrics that will be used during neural network training;
            :param monitor_callback: (str) specify which metric will be monitored during network validation;
            :param save_best_only: (boolean) if True specified, then save weights only if the accuracy of the network predictions on the validation data has improved,
                False - in any case, save weights every epoch;
            :param save_weights_only: (boolean) if you want to save only network weights,use save_weights_only=True,
                use save_weights_only=False if you want to save weights and network architecture in one file
                                                                                            (not recommended);
            :param verbose_callback: (boolean) display network results on a validation data;
            :param optimizer: (str) optimizer algorithm.
            :param model_name: (str) name of model. It will be used in weights file.
            """

        self.nb_classes = nb_classes
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.frame_size = (frame_width, frame_height)
        self.channels_num = channels_num

        self.batch_size = batch_size

        self.nb_epoch = nb_epoch
        self.lr = lr
        self.normalize = normalize
        self.frame_count = frame_count
        self.weights_folder = weights_folder

        self.in_ram = dict()

        self.loss = loss
        self.metrics = metrics
        self.monitor_callback = monitor_callback
        self.save_best_only = save_best_only
        self.save_weights_only = save_weights_only
        self.verbose_callback = verbose_callback
        self.optimizer = optimizer

        self.model = None
        self.img_dtype = img_dtype
        self.model_name = model_name

    def load_images_in_ram(self,
                           path_list,
                           normalize_images=True,
                           alpha=0.98,
                           verbose=True):
        """The method loads data to RAM.

        :param path_list: (list) absolute path list,
        :param normalize_images: (boolean) If True, each video frame will normalize from -1 to 1,
                                        False - video frames will have initial range of values.
        :param alpha: (float) specified part of RAM, which will be used.
        :param verbose: (boolean) print information about loading data.
        """

        if verbose:
            print('Loading data in ram.')

        if alpha > 0.98 or alpha <= 0:
            alpha = 0.98
        reserved_ram_mb = alpha * psutil.virtual_memory().available
        rest_ram_mb = reserved_ram_mb
        images_count = len(path_list)
        for path_item_num, path_list_item in enumerate(path_list):

            # init values
            loaded_data = None
            data_size_mb = -1

            loaded_data = cv2.imread(path_list_item).astype(self.img_dtype)

            if normalize_images:
                loaded_data = (loaded_data - 127.5) / 127.5

            data_size_mb = sys.getsizeof(loaded_data)
            if rest_ram_mb >= data_size_mb:
                if not (loaded_data is None):
                    self.in_ram[path_list_item] = loaded_data
                    rest_ram_mb -= data_size_mb
                    if verbose:
                        print('%d of %d images loaded.' % (path_item_num + 1, images_count))
                else:
                    available_ram_mb = psutil.virtual_memory().available
                    if verbose:
                        print("%s was not loaded. %d megabytes available" % (path_list_item,
                                                                             available_ram_mb / 1024 ** 2))
                if verbose:
                    print(str(rest_ram_mb / 1024 ** 2) + " megabytes rest.")

            if rest_ram_mb // 1024 ** 2 == 0:
                break
        if verbose:
            print("%d items were loaded in ram. %d megabytes was reserved." % (len(self.in_ram),
                                                                               (reserved_ram_mb - rest_ram_mb) /
                                                                               1024 ** 2))

    def generate_labeled_image_samples(self, x_data, y_data=None, verbose=True):
        """Generates batch_size samples from x_data and y_data.
        :param x_data: (ndarray) list of video paths;
        :param y_data: (ndarray) expected label output;
        :param verbose: (bool) print info messages;
        :return:if the network in training mode, it will yield batch consists of tuple of inputs and expected outputs,
                 only inputs if the network in prediction mode.
        """

        while True:
            steps = len(x_data) // self.batch_size

            for step in range(steps):
                begin_batch = step * self.batch_size
                end_batch = (step + 1) * self.batch_size

                np_images = np.empty((0, self.frame_size[1], self.frame_size[0], self.channels_num))

                for x_batch_item in x_data[begin_batch: end_batch]:
                    image = None
                    verbose_message = ''
                    if not (x_batch_item in self.in_ram.keys()):
                        verbose_message = "Loading from HDD: %s"
                        image = cv2.imread(x_batch_item)

                        if self.normalize:
                            image = (image - 127.5) / 127.5
                            image = image.astype(self.img_dtype)

                    else:
                        verbose_message = "Loading from RAM: %s"
                        image = self.in_ram[x_batch_item]

                    if verbose:
                        print(verbose_message % x_batch_item)

                    if image.shape[0] != self.frame_height or image.shape[1] != self.frame_width:
                        image = cv2.resize(image, self.frame_size)

                    image = np.expand_dims(image, 0)
                    np_images = np.concatenate((np_images, image), 0)

                if y_data is None:
                    yield np_images

                else:
                    yield np_images, y_data[begin_batch: end_batch]

    def build(self):
        base_model = Xception(input_shape=(self.frame_height, self.frame_width, self.channels_num),
                              include_top=False,
                              weights='imagenet')
        base_output = base_model.output

        global_average_pooling_layer = GlobalAveragePooling2D()(base_output)
        output = Dense(self.nb_classes, activation='softmax')(global_average_pooling_layer)

        self.model = Model(base_model.input, output)

    def train(self, x_train, y_train, x_val, y_val):

        model = self.model
        # compile model
        model.compile(loss=self.loss,
                      optimizer=self.optimizer,
                      metrics=self.metrics)
        k.set_value(model.optimizer.lr, self.lr)

        # set callbacks
        model_weights_description = self.model_name + '_' + str(self.frame_width) + '_' + str(self.frame_height)
        weights_file_name = model_weights_description + '_{epoch:02d}_{' + self.monitor_callback + ':.4f}.h5'
        weights_path = Path(self.weights_folder).joinpath(weights_file_name)
        callbacks_list = [ModelCheckpoint(str(weights_path),
                                          monitor=self.monitor_callback,
                                          verbose=self.verbose_callback,
                                          save_best_only=self.save_best_only,
                                          save_weights_only=self.save_weights_only)]

        # train model
        model.fit_generator(self.generate_labeled_image_samples(x_train, y_train, False),
                            steps_per_epoch=len(x_train) // self.batch_size,
                            validation_data=self.generate_labeled_image_samples(x_val, y_val),
                            validation_steps=len(x_val) // self.batch_size,
                            epochs=self.nb_epoch,
                            callbacks=callbacks_list,
                            verbose=True)

    def predict(self, x_test):
        predictions = self.model.predict_generator(self.generate_labeled_image_samples(x_test),
                                                   steps=len(x_test) // self.batch_size,
                                                   verbose=1)
        return predictions