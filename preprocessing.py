"""This file stores functions for image preprocessing and data augmentation.
Note that before run preprocessing.py, you must: 
1. Move all images of subdirectories IDRND_FASDB_train/real, IDRND_FASDB_train/spoof, IDRND_FASDB_val/real,
IDRND_FASDB_val/spoof to the train/real and train/spoof folders.
2. Remove all images which names starts with ._ (dot and underscore)."""

from pathlib import Path
import random
import cv2


def rand_crop(img, rand_crop_width, rand_crop_height):
    """This function crops random area of specified size of specified image
    :param img: (ndarray) specified image;
    :param rand_crop_width: (int) width of crop;
    :param rand_crop_height: (int) height of crop;
    :return: (ndarray) crop of image.
    """
    rand_y = random.randint(0, img.shape[0] - rand_crop_width)
    rand_x = random.randint(0, img.shape[1] - rand_crop_height)
    img_crop = img[rand_y: rand_y + rand_crop_height, rand_x: rand_x + rand_crop_width]
    return img_crop


def get_augmented_crops(img, img_width, img_height):
    """This function generates four flipped horizontally and vertically crops of specified image
    :param img: (ndarray) input image;
    :param img_width: (int) width of crops;
    :param img_height: (int) height of crops;
    :return: list of flipped horizontally and vertically crops.
    """
    crop_1 = rand_crop(img, img_width, img_height)
    crop_1_flip_v = cv2.flip(crop_1, 0)
    crop_2_flip_h = cv2.flip(crop_1, 1)
    crop_3_flip_vh = cv2.flip(cv2.flip(crop_1, 0), 1)
    return crop_1, crop_1_flip_v, crop_2_flip_h, crop_3_flip_vh


if __name__ == "__main__":
    train_dir = Path.cwd().joinpath('train')
    real_dir = train_dir.joinpath('real')
    spoof_dir = train_dir.joinpath('spoof')

    test_dir = Path.cwd().joinpath('test')
    real_path_list = sorted([str(p) for p in list(real_dir.glob('*.png'))])
    spoof_path_list = sorted([str(p) for p in list(spoof_dir.glob('*.png'))])

    # there is imbalance in the data set
    real_img_count = len(real_path_list)
    spoof_img_count = len(spoof_path_list)
    print(real_img_count)
    print(spoof_img_count)

    # We need to reduce the difference between number of classes' samples.
    # We will expand number of real images using augmentation methods.
    # We will pick up crops of random areas of image and flip they vertically and horizontally

    crops_dir = Path.cwd().joinpath('crops300')
    augmented_real = crops_dir.joinpath('real')
    crops_spoof = crops_dir.joinpath('spoof')

    crop_width = 300
    crop_height = 300

    img_counter = 1

    print("Augmenting data...")
    # augment train
    for real_img_path in real_dir.glob('*.png'):

        real_img = cv2.imread(str(real_img_path), -1)
        if real_img is None or real_img.shape[0] < crop_height or real_img.shape[1] < crop_width:
            continue
        crop_1, crop_2, crop_1_flip_v, crop_2_flip_h = get_augmented_crops(real_img, crop_width, crop_height)

        # write crops
        cv2.imwrite(str(augmented_real.joinpath(real_img_path.name.split('.')[0] + '_crop1.png')), crop_1)
        cv2.imwrite(str(augmented_real.joinpath(real_img_path.name.split('.')[0] + '_flip_v_crop1.png')), crop_1_flip_v)
        cv2.imwrite(str(augmented_real.joinpath(real_img_path.name.split('.')[0] + '_flip_h_crop1.png')), crop_2_flip_h)
        cv2.imwrite(str(augmented_real.joinpath(real_img_path.name.split('.')[0] + '_flip_vh_crop1.png')), crop_2)

        if img_counter % 200 == 0:
            print("Images processed: %d of %d" % (img_counter, real_img_count))

        img_counter += 1

    img_counter = 1
    print("Cropping spoof images...")
    for spoof_img_path in spoof_dir.glob('*.png'):
        spoof_img = cv2.imread(str(spoof_img_path))
        if spoof_img is None or spoof_img.shape[0] < crop_height or spoof_img.shape[1] < crop_width:
            continue
        cv2.imwrite(str(crops_spoof.joinpath(spoof_img_path.name.split('.')[0] + '_crop.png')),
                    rand_crop(spoof_img, crop_width, crop_height))
        if img_counter % 200 == 0:
            print("%d of %d spoof images had been cropped." % (img_counter, spoof_img_count))
        img_counter += 1
